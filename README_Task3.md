# Consider a scenario where we have created a feature branch from our master branch where you have to create the html file to display ninja's name and batch details, but you realized that the readme has to go first in order for any new member as you are already in between the creation of your html file you cannot send it to your remote branch, come up with a strategy where you can preserve your changes and first create the readme file and only after the readme is pushed then you resume your work back on the html file, use the appropriate git operations for the same

![](screenshots/10.png)

## Using git stash to achieve desired results

![](screenshots/11.png)

![](screenshots/12.png)

## Pushing readme first.

![](screenshots/13.png)

![](screenshots/14.png)

## Using git stash apply to start working on ninjaNameBatch.html again and pushing it to remote repo after our work is done

![](screenshots/15.png)

![](screenshots/16.png)
