# Consider Creating a html file to display the Name of the ninja and the batch number after the html is created and pushed, create a readme file for that html but after adding it you realized that we also have to mention the topics covered in the html file and we want to put those changes in the same last commit where html was originally pushed. Use the git operations suitable for this scenario.

![](screenshots/3.png)

![](screenshots/5.png)

![](screenshots/6.png)

## Using git commit --amend to put the new file in the same commit.

![](screenshots/7.png)

## After runnig git commit --amend we have to run git pull and then git push to be able to push our changes in remote repository.

![](screenshots/8.png)

![](screenshots/9.png)

