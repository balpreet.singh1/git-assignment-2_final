# Consider working on a file for which you maintain a document as well in which you are keeping the steps that you are performing for troubleshooting and you have accidentally added that file in your local repository and you have to get that file out from your local but not from your file system. Use the git operations suitable for this scenario.

![](screenshots/1.png)


## Using git reset and .gitignore to get the file troubleshootSteps.txt out from local repo but not from file system.

## We have to add the filename troubleshootSteps.txt in .gitignore to make the file untrackable by git.

![](screenshots/2.png)

## Here we can see after running ls command the file troubleshootSteps.txt is still in the local file system but not in the local repo.
